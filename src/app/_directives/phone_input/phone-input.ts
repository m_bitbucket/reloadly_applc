import { OnInit, Input, Renderer2, ElementRef, Directive, AfterViewInit } from '@angular/core';
declare const $: any;
declare const swal: any;

@Directive({
  selector: '[phoneInput]'
})

export class PhoneInputDirective implements OnInit, AfterViewInit {
  @Input('appTest') appTest;
  elem: HTMLElement;
  constructor(
    private render: Renderer2,
    private elRef: ElementRef
  ) {
    this.elem = this.elRef.nativeElement;
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    const phone_plugin = $(this.elem);
    phone_plugin.intlTelInput({
      initialCountry: 'auto',
      geoIpLookup: function(callback) {
        $.get('https://ipinfo.io', function() {}, 'jsonp').always(resp => {
          phone_plugin.intlTelInput('setCountry', resp.country.toLowerCase());
        });
      },
      utilsScript: './assets/js/utils.js',
    });
  }
}



