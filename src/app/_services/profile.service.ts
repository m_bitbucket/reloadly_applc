import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class ProfileService {
  host: string = environment.host;

  public profile = null;
  public user_id = null;
  public access_token = null;

  httpOptions: any;
  jwt_helper = new JwtHelperService();

  constructor(
    private http: HttpClient
  ) { }

  set set_profile(data) {
    this.profile = data;
  }

  get_profile() {
    if (!this.profile) {
      this.access_token = localStorage.getItem('access_token');
      this.user_id = this.jwt_helper.decodeToken(this.access_token)['https://portal.reloadly.com/id'];
      this.httpOptions = new HttpHeaders({
        'Accept': 'application/vnd.reloadly.app-v1+json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.access_token
      });
      // headers.append('Authorization', 'Bearer ' + this.access_token);
      return this.http.get(this.host + `/accounts/users/${this.user_id}/profile`, {headers: this.httpOptions}).pipe(
        tap( res => this.profile = res)
      );
    }
    return of(this.profile);
  }

  update_profile(data) {
    return this.http.patch(this.host + `/accounts/users/${this.user_id}/profile`, data, {headers: this.httpOptions}).pipe(
      tap( res => this.profile = res)
    );
  }

  passwordResetStep1(data: string) {
    const header = new HttpHeaders({
      'Accept': 'application/vnd.reloadly.app-v1+json',
      'Content-Type': 'application/json'
    });
    return this.http.post(this.host + `/accounts/reset-password`, {email: data}, {headers: header});
  }

  passwordResetStep2(data) {
    const header = new HttpHeaders({
      'Accept': 'application/vnd.reloadly.app-v1+json',
      'Content-Type': 'application/json'
    });
    const _data = {
      'email': data.email,
      'password': data.password,
      'passwordConfirmation': data.confirm,
      'verificationToken': 'string'
    };
    return this.http.post(this.host + `/accounts/reset-password`, _data, {headers: header});
  }

}
