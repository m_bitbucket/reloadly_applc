import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import {SocialOnboardModel, NoNSocialOnboardModel} from '@app/_model';

import { AuthService } from './auth.service';

@Injectable()

export class OnboardService {
  host: string = environment.host;
  httpOptions: any = {};
  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.httpOptions = authService.httpOptions;
  }

  social(data: SocialOnboardModel) {
    return this.http.post(`${this.host}accounts/users/on-board/social`, data, this.httpOptions);
  }

  non_social(data: NoNSocialOnboardModel) {
    return this.http.post(`${this.host}accounts/users/${this.authService.user_id}/on-board`, data, this.httpOptions);
  }
}
