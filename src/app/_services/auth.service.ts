import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '@env/environment';
import { AUTH_CONFIG } from '@app/config';
import { JwtHelperService } from '@auth0/angular-jwt';

import {of} from 'rxjs/observable/of';
import { tap } from 'rxjs/operators';

import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {
  host: string = environment.host;
  access_token: string;
  user_id: number;
  jwt_helper = new JwtHelperService();
  httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/vnd.reloadly.app-v1+json',
      'Content-Type': 'application/json'
    })
  };



  // auth0 = new auth0.WebAuth({
  //   clientID: 'KisvuWgWFtXGuNChOIasBGZCtkSoyqUz',
  //   domain: 'test-my-own.eu.auth0.com',
  //   responseType: 'token',
  //   prompt: 'none',
  //   audience: 'https://test-my-own.eu.auth0.com/userinfo',
  //   redirectUri: 'http://localhost:3000'
  // });

  auth0 = new auth0.WebAuth({
    clientID: AUTH_CONFIG.clientID,
    domain: AUTH_CONFIG.domain,
    responseType: 'token',
    prompt: 'none',
    audience: AUTH_CONFIG.audience,
    redirectUri: AUTH_CONFIG.callbackURL
  });

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  public handleAuthentication(): void {
    // if (localStorage.getItem('access_token')) {
    //   this.set_session(localStorage.getItem('access_token'));
    //   return;
    // }
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken /*&& authResult.idToken*/) {
        const auth_res = this.jwt_helper.decodeToken(authResult.accessToken);
        console.warn('auth_res', auth_res);
        if (auth_res.hasOwnProperty('https://portal.reloadly.com/socialUserId')) {
          this.on_board_social(auth_res);
        }
        this.set_session(authResult);
      } else if (err) {
        console.warn(err);
        // alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  register(data) {
    return this.http.post(this.host + '/accounts/register', data,  this.httpOptions);
  }

  set_session(authResult) {
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('expires_at', JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime()));
    this.access_token = authResult.accessToken;
    // this.user_id = this.jwt_helper.decodeToken(authResult.accessToken)[AUTH_CONFIG.audience + '/id'];
  }

  on_board_social(data) {

  }

  log_in(data) {
    // this.auth0.login({
    //   realm: 'PortalServiceConnection-STAGE',
    //   username: data.login,
    //   password: data.password,
    // }, res => {
    //    return of(res);
    // });
    this.auth0.authorize();
  }
  log_in_fc() {
    this.auth0.authorize({
      connection: 'facebook',
      audience: AUTH_CONFIG.audience
    });
  }

  public logout() {
    // this.http.get(AUTH_CONFIG.audience + '/v2/logout').subscribe(
    //   res => console.log(res),
    //   err => console.log(err)
    // );
    this.auth0.logout({
      returnTo: AUTH_CONFIG.callbackURL,
      clientID: AUTH_CONFIG.clientID,
      federated: 'Facebook'
    });

    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_at');
    // this.router.navigate(['/login']);
  }

  // login() {
  //   this.access_token = localStorage.getItem('access_token');
  //   this.user_id = this.jwt_helper.decodeToken(this.access_token)['https://portal.reloadly.com/id'];
  // }
}
