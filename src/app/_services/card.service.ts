import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { AuthService } from './auth.service';

@Injectable()

export class CardService {
  host: string = environment.host;
  user_id: number;
  httpOptions: {};
  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.user_id = this.authService.user_id;
    this.httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/vnd.reloadly.app-v1+json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.access_token
      })
    };
  }

  get_cards() {
    return this.http.get(this.host + '/accounts/users/' + this.user_id + '/cards', this.httpOptions);
  }

  add_card(id: string) {
    const data = {
      'defaultCard': false,
      'token': id
    };
    return this.http.post(this.host + '/accounts/users/' + this.user_id + '/cards', data , this.httpOptions);
  }
}
