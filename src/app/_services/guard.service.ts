import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { ProfileService } from './profile.service';
import { environment } from '@env/environment';

@Injectable()

export class GuardService implements CanActivate {
  constructor(
    private profileService: ProfileService,
    private router: Router
  ) {}

  canActivate() {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    const state = new Date().getTime() < expiresAt;
    if (!state) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
