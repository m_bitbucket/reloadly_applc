import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@env/environment';

import {of} from 'rxjs/observable/of';
import { tap } from 'rxjs/operators';

import { AuthService } from '@app/_services/auth.service';

@Injectable()
export class AddressService {
  host: string = environment.host;
  access_token: string;
  user_id: number;

  httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.user_id = this.authService.user_id;
    this.access_token = this.authService.access_token;
    this.httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/vnd.reloadly.app-v1+json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.access_token
      })
    };
  }

  get_addresses() {
    return this.http.get(`${this.host}/accounts/users/${this.user_id}/addresses`, this.httpOptions);
  }

  add_address(data: any) {
    return this.http.post(`${this.host}/accounts/users/${this.user_id}/addresses`, data, this.httpOptions);
  }
}
