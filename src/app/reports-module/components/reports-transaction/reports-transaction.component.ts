import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const moment: any;

@Component({
  selector: 'app-reports-transaction',
  templateUrl: './reports-transaction.component.html',
  styleUrls: ['./reports-transaction.component.scss']
})
export class ReportsTransactionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.selectpicker').selectpicker();
    $('.input-daterange-datepicker').daterangepicker({
      maxDate: moment().format('L')
    });
  }

}
