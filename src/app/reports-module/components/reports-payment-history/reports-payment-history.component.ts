import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const moment: any;


@Component({
  selector: 'app-reports-payment-history',
  templateUrl: './reports-payment-history.component.html',
  styleUrls: ['./reports-payment-history.component.scss']
})
export class ReportsPaymentHistoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.selectpicker').selectpicker();
    $('.input-daterange-datepicker').daterangepicker({
      maxDate: moment().format('L')
    });
  }

}
