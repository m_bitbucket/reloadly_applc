import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsPaymentHistoryComponent } from './reports-payment-history.component';

describe('ReportsPaymentHistoryComponent', () => {
  let component: ReportsPaymentHistoryComponent;
  let fixture: ComponentFixture<ReportsPaymentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsPaymentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
