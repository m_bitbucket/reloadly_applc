import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsTransactionComponent } from './components/reports-transaction/reports-transaction.component';
import { ReportsPaymentHistoryComponent } from './components/reports-payment-history/reports-payment-history.component';


const routes: Routes = [
  { path: 'transaction', component: ReportsTransactionComponent },
  { path: 'payment_history', component: ReportsPaymentHistoryComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ReportsRoutingModule {}
