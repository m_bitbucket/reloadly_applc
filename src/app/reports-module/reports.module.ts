import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';

import { ReportsComponent } from './reports.component';
import { ReportsTransactionComponent } from './components/reports-transaction/reports-transaction.component';
import { ReportsPaymentHistoryComponent } from './components/reports-payment-history/reports-payment-history.component';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule
  ],
  declarations: [
    ReportsComponent,
    ReportsTransactionComponent,
    ReportsPaymentHistoryComponent
  ]
})
export class ReportsModule { }
