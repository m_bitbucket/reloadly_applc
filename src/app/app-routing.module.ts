import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './core-module/page-not-found/page-not-found.component';
import { LoginComponent } from './core-module/login/login.component';
import {
  OnboardingComponent,
  SuccessComponent,
  RegisterComponent
} from './core-module';

import { GuardService, AuthService } from '@app/_services';

const routes: Routes = [

  { path: 'dashboard', loadChildren: './dashboard-module/dashboard.module#DashboardModule',  canActivate: [ GuardService ]},
  { path: 'profile', loadChildren: './profile-module/profile.module#ProfileModule',   canActivate: [ GuardService ]},
  { path: 'reports', loadChildren: './reports-module/reports.module#ReportsModule',  canActivate: [ GuardService ]},
  { path: 'discount', loadChildren: './discount-module/discount.module#DiscountModule',  canActivate: [ GuardService ]},
  { path: 'balance', loadChildren: './balance-module/balance.module#BalanceModule',  canActivate: [ GuardService ]},
  { path: 'setting', loadChildren: './setting-module/setting.module#SettingModule',  canActivate: [ GuardService ]},
  { path: 'api', loadChildren: './api-module/api.module#ApiModule',  canActivate: [ GuardService ]},
  { path: 'docs', loadChildren: './docs-module/docs.module#DocsModule', canActivate: [ GuardService ]},
  { path: 'login', pathMatch: 'full', component: LoginComponent},
  { path: 'registration', pathMatch: 'full', component: RegisterComponent},
  { path: 'onboarding', pathMatch: 'full', component: OnboardingComponent },
  { path: 'success', component: SuccessComponent },
  // { path: 'topup', loadChildren: './topup-module/topup.module#TopupModule'},
  // { path: 'test', component: PageNotFoundComponent},
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', canActivate: [ GuardService ] },
  { path: '**', redirectTo: 'dashboard', canActivate: [ GuardService ] },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
