import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DashboardService {

  constructor(
    private http: HttpClient
  ) { }

  get_instagram() {
    return this.http.get('https://api.instagram.com/v1/users/1643960022/media/recent/?access_token=1643960022.f39a0c9.48d3b49386dc4871bbee73cc51a78238');
  }
}
