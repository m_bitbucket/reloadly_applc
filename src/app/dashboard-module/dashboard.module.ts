import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { DashboardTotalComponent } from './components/dashboard-total/dashboard-total.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardChartComponent } from './components/dashboard-chart/dashboard-chart.component';
import { DashboardSubscribeComponent } from './components/dashboard-subscribe/dashboard-subscribe.component';
import { DashboardApiCredentialsComponent } from './components/dashboard-api-credentials/dashboard-api-credentials.component';
import { IntercomComponent } from './components/intercom/intercom.component';
import { DashboardTableComponent } from './components/dashboard-table/dashboard-table.component';
import { DashboardSalesByProductComponent } from './components/dashboard-sales-by-product/dashboard-sales-by-product.component';

import { DashboardService } from './_services';
import { DashboardPopUpComponent } from './components/dashboard-pop-up/dashboard-pop-up.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule
  ],
  declarations: [
    DashboardComponent,
    DashboardTotalComponent,
    DashboardChartComponent,
    DashboardSubscribeComponent,
    IntercomComponent,
    DashboardSubscribeComponent,
    DashboardApiCredentialsComponent,
    DashboardTableComponent,
    DashboardSalesByProductComponent,
    DashboardPopUpComponent
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule { }
