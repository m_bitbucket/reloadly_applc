import { Component, OnInit } from '@angular/core';

declare const Intercom: any;
declare const window: any;

@Component({
  selector: 'app-intercom',
  templateUrl: './intercom.component.html',
  styleUrls: ['./intercom.component.scss']
})
export class IntercomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.Intercom("boot", {
     app_id: "osrlvrzy",
      name: "Tetst name", // Full name
      email: "customer@example.com", // Email address
     created_at: Date.now() // Signup date as a Unix timestamp
    });
   window.Intercom("update");

    //const timeout = setTimeout(() => clearInterval(interval), 30000);
    //const interval = setInterval(() => {
      //const iframe = document.querySelector('#intercom-container');

      // if (iframe) {
      //   Append the stylesheet to the iframe head
      //   iframe.contentDocument.head.appendChild(stylesheet);
      //   $('#wer').append(iframe);
      //   clearInterval(interval);
      //   clearTimeout(timeout);
      // }
    //}, 100);
  }

}
