import { Component, OnInit } from '@angular/core';
declare const $:any;

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss']
})
export class DashboardTableComponent implements OnInit {
  notificationsData: string = 'Some additional information';
  constructor() { }

  ngOnInit() {
  }
  displayInfo(data) {
    this.notificationsData = 'Some additional information ' + data; 
  }
}
