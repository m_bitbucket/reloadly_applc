import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSubscribeComponent } from './dashboard-subscribe.component';

describe('DashboardSubscribeComponent', () => {
  let component: DashboardSubscribeComponent;
  let fixture: ComponentFixture<DashboardSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
