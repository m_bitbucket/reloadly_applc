import { Component, OnInit, AfterViewInit} from '@angular/core';
import { DashboardService } from '../../_services/dashboard.service';
declare const $: any;

@Component({
  selector: 'app-dashboard-subscribe',
  templateUrl: './dashboard-subscribe.component.html',
  styleUrls: ['./dashboard-subscribe.component.scss']
})
export class DashboardSubscribeComponent implements OnInit, AfterViewInit {
  inst: any = {
    data: {},
    imgs: []
  };
  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.dashboardService.get_instagram().subscribe(
      (res: any) => {
        res.data.forEach(el => this.inst.imgs.push( {img: el.images.standard_resolution.url, url: el.link} ));
        this.inst.data = res.data[0].user;
      },
      err => console.error(err)
    );
  }
  ngAfterViewInit() {
    !function(d, s, id){
      let js: any;
      const fjs = d.getElementsByTagName(s)[0],
        p = 'https';
        js = d.createElement(s);
        js.id = id;
        js.src = p + '://platform.twitter.com/widgets.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'twitter-wjs');
  }

}
