import { Component, OnInit, ViewEncapsulation } from '@angular/core';
declare const $: any;
declare const Chartist: any;
declare const c3: any;
declare const moment: any;

@Component({
  selector: 'app-dashboard-sales-by-product',
  templateUrl: './dashboard-sales-by-product.component.html',
  styleUrls: ['./dashboard-sales-by-product.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardSalesByProductComponent implements OnInit {
  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];
  dayGraphInfo = {
    labels: [],   //1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30
    series: [
      [5, 1, 7, 5, 10, 6, 8, 6, 5, 1, 7, 5, 10, 6, 8, 6,5, 1, 7, 5, 10, 6, 8, 6, 5, 1, 7, 5, 10, 6, 4]
      , [2, 6, 2, 7, 23, 3, 5, 12, 2, 6, 2, 7, 23, 3, 5, 12, 2, 6, 2, 7, 23, 3, 5, 12, 2, 6, 2, 7, 23, 3, 5]
    ]
  }
  weekGraphInfo = {
    labels: [],   //1, 2, 3, 4, 5, 6, 7, 8
    series: [
      [1, 3, 6, 8, 15, 9, 10, 20]
      , [1, 6, 1, 2, 5, 9, 1, 5]
    ]
  }
  monthGraphInfo = {
    labels: [],   //1, 2, 3, 4, 5, 6, 7, 8
    series: [
      [0, 5, 6, 8, 25, 9, 8, 24]
      , [0, 3, 1, 2, 8, 1, 5, 1]
    ]
  }
  constructor() { }

  ngOnInit() {
    let eightMonthAgo = new Date();
    eightMonthAgo.setMonth(eightMonthAgo.getMonth() - 8);
    this.monthGraphInfo.labels = ['','','','','','','',''].map(v => {
      eightMonthAgo.setMonth(eightMonthAgo.getMonth() + 1);
        return this.monthNames[eightMonthAgo.getMonth()];
      });

    let eightweeksago = new Date();
    eightweeksago.setDate(eightweeksago.getDate() - 56);
    this.weekGraphInfo.labels = ['','','','','','','',''].map(v => {
      eightweeksago.setDate(eightweeksago.getDate() + 7);
      return this.monthNames[eightweeksago.getMonth()] + ' ' + eightweeksago.getDate();
     });

     let thirtyDaysAgo = new Date();
     thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);
     this.dayGraphInfo.labels = ['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''].map(v => {
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() + 1);
      return thirtyDaysAgo.getDate() == 1 ? this.monthNames[thirtyDaysAgo.getMonth()] + ' ' + thirtyDaysAgo.getDate() : thirtyDaysAgo.getDate();
     });

    this.drawGraph(this.dayGraphInfo);
    $('#default-button').addClass('active');
  }
  drawGraph(data) {
    $(function () {
      var chart = new Chartist.Line('.website-visitor', {
        labels: data.labels,
        series: data.series
      }, {
          low: 0,
          high: 28,
          showArea: true,
          fullWidth: true,
          plugins: [
            Chartist.plugins.tooltip()
          ],
          axisY: {
            onlyInteger: true
            , scaleMinSpace: 40
            , offset: 20
            , labelInterpolationFnc: function (value) {
              return (value / 1) + 'k';
            }
          },
        });

      // Offset x1 a tiny amount so that the straight stroke gets a bounding box
      // Straight lines don't get a bounding box
      // Last remark on -> http://www.w3.org/TR/SVG11/coords.html#ObjectBoundingBox
      chart.on('draw', function (ctx) {
        if (ctx.type === 'area') {
          ctx.element.attr({
            x1: ctx.x1 + 0.001
          });
        }
      });

      // Create the gradient definition on created event (always after chart re-render)
      chart.on('created', function (ctx) {
        var defs = ctx.svg.elem('defs');
        defs.elem('linearGradient', {
          id: 'gradient',
          x1: 0,
          y1: 1,
          x2: 0,
          y2: 0
        }).elem('stop', {
          offset: 0,
          'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
          offset: 1,
          'stop-color': 'rgba(38, 198, 218, 1)'
        });
      });

    });
  }

}
