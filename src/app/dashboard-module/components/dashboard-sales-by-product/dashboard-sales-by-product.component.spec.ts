import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSalesByProductComponent } from './dashboard-sales-by-product.component';

describe('DashboardSalesByProductComponent', () => {
  let component: DashboardSalesByProductComponent;
  let fixture: ComponentFixture<DashboardSalesByProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSalesByProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSalesByProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
