import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPopUpComponent } from './dashboard-pop-up.component';

describe('DashboardPopUpComponent', () => {
  let component: DashboardPopUpComponent;
  let fixture: ComponentFixture<DashboardPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
