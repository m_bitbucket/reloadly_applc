import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-pop-up',
  templateUrl: './dashboard-pop-up.component.html',
  styleUrls: ['./dashboard-pop-up.component.scss']
})
export class DashboardPopUpComponent implements OnInit {
@Input() data;
  constructor() { }

  ngOnInit() {
  }

}
