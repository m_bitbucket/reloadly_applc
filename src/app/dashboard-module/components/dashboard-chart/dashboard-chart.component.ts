import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const Chartist: any;
declare const c3: any;
declare const moment: any;

@Component({
  selector: 'app-dashboard-chart',
  templateUrl: './dashboard-chart.component.html',
  styleUrls: ['./dashboard-chart.component.scss']
})
export class DashboardChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.input-daterange-datepicker').daterangepicker({
      maxDate: moment().format('L')
    });
    $(function () {

      // ==============================================================
      // Sales overview
      // ==============================================================
      new Chartist.Line('#sales-overview2', {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul']
        , series: [
          {meta:"Earning ($)", data: [0, 150, 110, 240, 200, 200, 300]}
        ]
      }, {
        low: 0
        , high:400
        , showArea: true
        , divisor: 10
        , lineSmooth:false
        , fullWidth: true
        , showLine: true
        , chartPadding: 30
        , axisX: {
          showLabel: true
          , showGrid: false
          , offset: 50
        }
        , plugins: [
          Chartist.plugins.tooltip()
        ],
        // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
        axisY: {
          onlyInteger: true
          , showLabel: true
          , scaleMinSpace: 50
          , showGrid: true
          , offset: 10,
          labelInterpolationFnc: function(value) {
            return (value / 100) + 'k'
          },

        }

      });
      // ==============================================================
      // Visitor
      // ==============================================================

      var chart = c3.generate({
        bindto: '#visitor',
        data: {
          columns: [
            ['Other', 30],
            ['Desktop', 10],
            ['Tablet', 40],
            ['Mobile', 50],
          ],

          type : 'donut',
          onclick: function (d, i) { console.log("onclick", d, i); },
          onmouseover: function (d, i) { console.log("onmouseover", d, i); },
          onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        },
        donut: {
          label: {
            show: false
          },
          title:"Visits",
          width:20,

        },

        legend: {
          hide: true
          //or hide: 'data1'
          //or hide: ['data1', 'data2']
        },
        color: {
          pattern: ['#eceff1', '#745af2', '#26c6da', '#1e88e5']
        }
      });

      // ==============================================================
      // Website Visitor
      // ==============================================================

    });
  }

}
