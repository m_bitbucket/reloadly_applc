import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardApiCredentialsComponent } from './dashboard-api-credentials.component';

describe('DashboardApiCredentialsComponent', () => {
  let component: DashboardApiCredentialsComponent;
  let fixture: ComponentFixture<DashboardApiCredentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardApiCredentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardApiCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
