import { Component, OnInit } from '@angular/core';
import { CardService } from '@app/_services';

declare const swal: any;
declare const $: any;

@Component({
  selector: 'app-make-payment',
  templateUrl: './make-payment.component.html',
  styleUrls: ['./make-payment.component.scss']
})
export class MakePaymentComponent implements OnInit {
  model: any = {};
  constructor(
    private cardService: CardService
  ) { }

  ngOnInit() {

  }

  make_payment(f) {
    $('.modal').modal('hide');
    swal('Payment!', '', 'success');
  }
}
