import { Component, OnInit } from '@angular/core';
import { CardService } from '@app/_services';

declare const $: any;
declare const swal: any;
declare const Stripe: any;

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss']
})
export class RechargeComponent implements OnInit {
  cards: any[];

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit() {

    // this.cards = [
    //   {
    //     img: './assets/img/cc-mastercard.png',
    //     name: 'michel fran',
    //     card: '223232323232',
    //     expiration: '2/22',
    //     status: false,
    //     action: '--',
    //     current: false
    //   }, {
    //     img: './assets/img/cc-mastercard.png',
    //     name: 'michel  francis',
    //     card: '**** **** **** *121',
    //     expiration: '2/23',
    //     status: true,
    //     action: '--',
    //     current: true
    //   }
    // ];

    this.cardService.get_cards().subscribe(
      (res: any) => {
        this.cards = res.content;
      },
      err => console.log(err)
    );

/*
    $('.tab-wizard').steps({
      headerTag: 'h6'
      , bodyTag: 'section'
      , transitionEffect: 'fade'
      , titleTemplate: '<span class="step">#index#</span> #title#'
      , labels: {
        finish: 'Submit'
      }
      , onFinished: function (event, currentIndex) {
        swal('Form Submitted!', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.');
      }
    });
*/
  }
  pick_card(card: any) {
    this.cards.forEach(el => el.current = false);
    card.current = true;
  }
  remove_card(card: any) {
    let index;
    this.cards.forEach((el, ind) => {
      if (card === el) {
        index = ind;
        return;
      }
    });
    this.cards.splice(index, 1);
  }
}
