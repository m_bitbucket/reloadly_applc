import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-balance-auto-recharge',
  templateUrl: './balance-auto-recharge.component.html',
  styleUrls: ['./balance-auto-recharge.component.scss']
})
export class BalanceAutoRechargeComponent implements OnInit {
  model: any = {};
  constructor() { }

  ngOnInit() {
    $('.selectpicker').selectpicker();
  }

  send(f: HTMLFormElement) {
    swal('Saved!', '', 'success');
  }
}
