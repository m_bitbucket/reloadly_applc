import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceAutoRechargeComponent } from './balance-auto-recharge.component';

describe('BalanceAutoRechargeComponent', () => {
  let component: BalanceAutoRechargeComponent;
  let fixture: ComponentFixture<BalanceAutoRechargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceAutoRechargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceAutoRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
