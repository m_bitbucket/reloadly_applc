import { Component, OnInit, AfterViewInit} from '@angular/core';
import { CardService } from '@app/_services';
import { STRIPE_CONFIG } from '@app/config';

declare const Stripe: any;
declare const swal: any;
declare const $: any;

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})

export class AddCardComponent implements OnInit, AfterViewInit {
  model: any = {};
  stripe: any;
  card: any;
  card_err = '';
  constructor(
    private cardService: CardService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    // stripe
    this.stripe = Stripe(STRIPE_CONFIG.key);
    const elements = this.stripe.elements();
    const style = {
      base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };
    this.card = elements.create('card', {style: style});
    this.card.mount('#card-element');
    this.card.addEventListener('change', event => {
      if (event.error) {
        this.card_err = event.error.message;
      } else {
        this.card_err = '';
      }
    });
  }

  add_card() {
    this.stripe.createToken(this.card).then( result => {
      if (result.error) {
        this.card_err = result.error.message;
      } else {
        this.cardService.add_card(result.token.card.id).subscribe(
          res => {
            console.log(res);
            $('.modal').modal('hide');
            swal('Card Added!', '', 'success');
          },
          err => {
            console.log(err.error.message);
            $('.modal').modal('hide');
            swal('Oops!', err.error.message, 'error');
          }
        );
      }
    });
  }
}
