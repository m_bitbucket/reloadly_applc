import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BalanceAutoRechargeComponent } from './components/balance-auto-recharge/balance-auto-recharge.component';
import { RechargeComponent } from './components/recharge/recharge.component';

const routes: Routes = [
  { path: 'auto_recharge', component: BalanceAutoRechargeComponent },
  { path: 'make-payment', component: RechargeComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class BalanceRoutingModule {}
