import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalanceRoutingModule } from './balance-routing.module';
import { FormsModule } from '@angular/forms';

import { BalanceAutoRechargeComponent } from './components/balance-auto-recharge/balance-auto-recharge.component';
import { RechargeComponent } from './components/recharge/recharge.component';
import { AddCardComponent } from './components/add-card/add-card.component';
import { MakePaymentComponent } from './components/make-payment/make-payment.component';

import { CardService } from '@app/_services';

@NgModule({
  imports: [
    CommonModule,
    BalanceRoutingModule,
    FormsModule
  ],
  declarations: [
    BalanceAutoRechargeComponent,
    RechargeComponent,
    AddCardComponent,
    MakePaymentComponent
  ],
  providers: [
    CardService
  ]
})
export class BalanceModule { }
