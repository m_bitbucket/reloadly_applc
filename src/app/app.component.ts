import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/filter';

import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from '@app/_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  no_head = false;

  constructor(
    private route: Router,
    private authService: AuthService
  ) {
    authService.handleAuthentication();
  }

  ngOnInit() {
    const hash = location.hash;
    // if (hash) {
    //   location.hash = '';
    // }
    this.route.events.filter( event => event instanceof NavigationEnd).subscribe( (e: NavigationEnd) => {
        /(\/login|\/registration|\/onboarding|\/success)/.test(e.url) ? this.no_head = true : this.no_head = false;
    });
    // this.authService.login();
  }
}
