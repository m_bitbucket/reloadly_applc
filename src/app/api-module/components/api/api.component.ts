import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.selectpicker').selectpicker();
  }

  send(f: HTMLFormElement) {
    f.reset();
    swal('Saved!', '', 'success');
  }
}
