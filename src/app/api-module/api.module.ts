import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiRoutingModule } from './api-routing.module';

import { ApiComponent } from './components/api/api.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    ApiRoutingModule,
    FormsModule
  ],
  declarations: [
    ApiComponent
  ]
})
export class ApiModule { }
