import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ProfileService } from '@app/_services';
import { PhoneInputDirective, TabComponent, TabsComponent } from '@app/_directives';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    PhoneInputDirective,
    TabComponent, TabsComponent
  ],
  exports: [
    PhoneInputDirective,
    TabComponent, TabsComponent
  ],
  providers: [
    ProfileService
  ]
})
export class SharedModule { }
