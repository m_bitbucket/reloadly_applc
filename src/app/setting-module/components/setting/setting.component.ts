import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  model: any = {};
  constructor() { }

  ngOnInit() {
    $('.selectpicker').selectpicker();
  }

  send(f: HTMLFormElement) {
    f.reset();
    swal('Saved!', '', 'success');
  }
}
