import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingRoutingModule } from './setting-routing.module';

import { SettingComponent } from './components/setting/setting.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SettingRoutingModule,
    FormsModule
  ],
  declarations: [
    SettingComponent
  ]
})
export class SettingModule { }
