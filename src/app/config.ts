interface AuthConfig {
  clientID: string;
  domain: string;
  audience: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
 clientID: '5WzNYCOnJSg873yXBe3QpPyTvejuqW2c',
 domain: 'reloadly-stage.auth0.com',
 audience: 'https://portal-stage.reloadly.com',

  // clientID: 'KisvuWgWFtXGuNChOIasBGZCtkSoyqUz',
  // domain: 'test-my-own.eu.auth0.com',
  // audience: 'https://test-my-own.eu.auth0.com',
  callbackURL: 'http://localhost:3000',
};

export const STRIPE_CONFIG = {
  key: 'pk_test_572btOmhchqohNoDGhSHiYqA'
};

export const API_CONFIG = {
  url: 'https://portal.reloadly.com'
};
