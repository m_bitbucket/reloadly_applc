import { HeaderComponent } from './header.component';
import { HeaderSearchComponent } from './header-search/header-search.component';
import { HeaderNotificationComponent } from './header-notification/header-notification.component';
import { HeaderMessagesComponent } from './header-messages/header-messages.component';
import { HeaderMegaMenuComponent } from './header-mega-menu/header-mega-menu.component';
import { HeaderLanguageComponent } from './header-language/header-language.component';
import { HeaderProfileComponent } from './header-profile/header-profile.component';

export const HeaderComponents = [
  HeaderComponent,
  HeaderSearchComponent,
  HeaderNotificationComponent,
  HeaderMessagesComponent,
  HeaderMegaMenuComponent,
  HeaderLanguageComponent,
  HeaderProfileComponent
];
