import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderMegaMenuComponent } from './header-mega-menu.component';

describe('HeaderMegaMenuComponent', () => {
  let component: HeaderMegaMenuComponent;
  let fixture: ComponentFixture<HeaderMegaMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderMegaMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMegaMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
