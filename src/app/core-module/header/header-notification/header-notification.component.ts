import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../../../_services/firebase.service';

@Component({
  selector: 'app-header-notification',
  templateUrl: './header-notification.component.html',
  styleUrls: ['./header-notification.component.scss']
})
export class HeaderNotificationComponent implements OnInit {
  list: any;
  constructor(
    private firebaseService: FirebaseService
  ) { }

  ngOnInit() {
    this.firebaseService.getItems().subscribe(
      res => {
        this.list = res;
      }
    );
  }

}
