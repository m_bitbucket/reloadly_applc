import { Component, OnInit } from '@angular/core';
declare const window: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    window.Intercom('boot', {
      app_id: 'zeqbdpp9',
      name: 'Jane Doe',
      email: 'customer@example.com',
      created_at: 1312182000
    });
    window.Intercom('update');
    // logout
    // window.Intercom("shutdown");
  }

}
