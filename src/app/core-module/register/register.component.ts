import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/_services';
declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit, AfterViewInit {
  model: any = {
    phone: '+'
  };
  t_err = null;
  constructor(
    private authService: AuthService,
    private route: Router
  ) { }
  ngAfterViewInit() {
    const phone_plugin = $('#phone');
    phone_plugin.intlTelInput({
      initialCountry: 'auto',
      geoIpLookup: function(callback) {
        $.get('https://ipinfo.io', function() {}, 'jsonp').always(resp => {
          phone_plugin.intlTelInput('setCountry', resp.country.toLowerCase());
        });
      },
      utilsScript: './assets/js/utils.js',
    });
  }
  ngOnInit() {

  }

  register(f: HTMLFormElement) {
    const data = {
      email: this.model.email,
      firstName: this.model.firstName,
      lastName: this.model.lastName,
      password: this.model.password,
      phone: {
        countryCode: ($('#phone').intlTelInput('getSelectedCountryData').iso2).toLocaleUpperCase(),
        number: parseInt(this.model.phone)
      }
    };
    this.authService.register(data).subscribe(
      res => {
        this.route.navigate(['/success']);
        f.reset();
      },
      err => {
        this.t_err = err.error.message;
      }
    );
  }
}
