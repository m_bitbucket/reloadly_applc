import { Component, OnInit } from '@angular/core';
import { ProfileService } from '@app/_services';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  model: any = {};
  next_step = false;
  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit() {
  }

  recover(f: HTMLFormElement) {
    f.reset();
    if (this.next_step) {
      this.profileService.passwordResetStep1(this.model).subscribe(
        res => {
          this.next_step = true;
        },
        err => console.log(err)
      );
      return;
    }
    this.profileService.passwordResetStep1(this.model.email).subscribe(
      res => {
        this.next_step = true;
      },
      err => console.log(err)
    );
  }
}
