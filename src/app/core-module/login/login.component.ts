import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/_services';
import { AuthModel } from '../../_model';

declare const $: any;
declare const window: any;
declare const FB: any;
declare const IN: any;
declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  model: AuthModel = <AuthModel>{};
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngAfterViewInit() {
    // window.fbAsyncInit = function() {
    //   FB.init({
    //     appId      : '315196372242999',
    //     cookie     : true,
    //     xfbml      : true,
    //     version    : 'v2.8'
    //   });
    //
    //   // FB.getLoginStatus(function(response) {
    //   //   statusChangeCallback(response);
    //   // });
    //
    // };






    // gapi.load('auth2', function(){
    //   // Retrieve the singleton for the GoogleAuth library and set up the client.
    //   const auth2 = gapi.auth2.init({
    //     client_id: '682907276225-69lbidi436j4va1eas68pmn35rj5mmqo.apps.googleusercontent.com',
    //     cookiepolicy: 'single_host_origin',
    //     // Request scopes in addition to 'profile' and 'email'
    //     // scope: 'additional_scope'
    //   });
    //
    //   auth2.attachClickHandler(document.getElementById('google_log'), {},
    //     function(googleUser) {
    //         console.warn(googleUser.getBasicProfile().getName());
    //     }, function(error) {
    //       console.warn(JSON.stringify(error, undefined, 2));
    //     });
    // });



  }

  ngOnInit() {
    $(function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
    $('#to-recover').on('click', function() {
      $('#loginform').slideUp();
      $('#recoverform').fadeIn();
    });
  }

  log_in(f: HTMLFontElement) {
    this.authService.log_in(this.model);
    // this.authService.auth(this.model).subscribe(
    //   res => {
    //     this.router.navigate(['profile']);
    //   },
    //   err => console.log(err)
    // );
  }
  log_fb() {
    this.authService.log_in_fc();
  }
  // log_fb() {
  //   FB.login(response => {
  //     this.authService.social_auth(response.authResponse.accessToken, 'FACEBOOK').subscribe(
  //       res => console.log(res),
  //       err => console.log(err)
  //     );
  //   });
  // }

  // log_linkedin() {
  //   IN.User.authorize(function(res) {
  //     console.log(res);
  //   }, function(soem){
  //     console.warn(soem);
  //   });
  // }

  log_git() {

  }
}
