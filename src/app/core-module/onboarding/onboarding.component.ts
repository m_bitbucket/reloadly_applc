import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

import { SocialOnboardModel, NoNSocialOnboardModel } from '@app/_model';
import { API_CONFIG } from '@app/config';
import { OnboardService } from '@app/_services';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})

export class OnboardingComponent implements OnInit {
  jwt_helper = new JwtHelperService();
  data: SocialOnboardModel | NoNSocialOnboardModel;
  social_user = false;
  model: any = {
    currencyCode: 0
  };
  t_err = null ;

  constructor(
    private onboardService: OnboardService,
    private router: Router,
  ) {}

  ngOnInit() {
    const access_token = localStorage.getItem('access_token');
    let data;
    if (access_token) {
      data = this.jwt_helper.decodeToken(access_token);
    }
    if (data.hasOwnProperty('https://portal.reloadly.com/isSocial')) {
      this.social_user = true;
      this.data =  {
        currencyCode: this.model.currencyCode,
        phone: this.model.phone,
        avatar: data[`${API_CONFIG.url}/avatar`],
        email: data[`${API_CONFIG.url}/email`],
        firstName: data[`${API_CONFIG.url}/firstName`],
        lastName: data[`${API_CONFIG.url}/lastName`],
        socialProvider: data[`${API_CONFIG.url}/socialProvider`],
        socialUserId: data[`${API_CONFIG.url}/socialUserI`]
      };
    } else {
      this.data = {
        currencyCode: this.model.currencyCode
      };
    }
  }

  send(f: HTMLFormElement) {
    if (this.social_user) {
      this.onboardService.social(<SocialOnboardModel>this.data).subscribe(
        res => {
          this.router.navigate(['/login']);
        },
        err => console.log(err)
      );
    } else {
      this.onboardService.non_social(<NoNSocialOnboardModel>this.data).subscribe(
        res => {
          this.router.navigate(['/login']);
        },
        err => console.log(err)
      );
    }
  }
}
