export class NoNSocialOnboardModel {
  'currencyCode': string | number;
}

export class SocialOnboardModel {
  'avatar': string;
  'currencyCode': string | number;
  'email': string;
  'firstName': string;
  'lastName': string;
  'phone': Phone;
  'socialProvider': string;
  'socialUserId': string;
}

export class Phone {
  'countryCode': string;
  'number': number;
}
