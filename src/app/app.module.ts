import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FacebookModule } from 'ngx-facebook';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { SharedModule } from '@app/_shared/modules/shared.module';
import { GuardService, OnboardService } from '@app/_services';


import { AppComponent } from './app.component';
import { HeaderComponents } from './core-module/header/index';
import { NavigationComponent } from './core-module/navigation/navigation.component';
import { FooterComponent } from './core-module/footer/footer.component';
import { PageNotFoundComponent } from './core-module/page-not-found/page-not-found.component';
import { LoginComponent } from './core-module/login/login.component';
import { RegisterComponent } from './core-module/register/register.component';
import { SuccessComponent } from './core-module';
import { RecoverPasswordComponent } from './core-module/login/recover-password/recover-password.component';
import { OnboardingComponent } from './core-module/onboarding/onboarding.component';



// FIREBASE
import { FirebaseConfig } from '@env/firebase.config';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { FirebaseService, AuthService } from './_services';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ...HeaderComponents,
    FooterComponent,
    PageNotFoundComponent,
    LoginComponent,
    RegisterComponent,
    RecoverPasswordComponent,
    OnboardingComponent,
    SuccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FacebookModule.forRoot(),
    AngularFireModule.initializeApp(FirebaseConfig.firebase, 'my-app-name'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    SharedModule
  ],
  providers: [
    AuthService,
    OnboardService,
    FirebaseService,
    AngularFireDatabase,
    GuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
