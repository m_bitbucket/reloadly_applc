import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ProfileRoutingModule } from './profile-routing.module';

import { ProfileComponent } from './profile.component';
import { ProfileOverviewComponent } from './components/profile-overview/profile-overview.component';
import { ProfileEditAccountComponent } from './components/profile-edit-account/profile-edit-account.component';
import { ProfileEditPasswordComponent } from './components/profile-edit-password/profile-edit-password.component';
import { ProfileEditAddressComponent } from './components/profile-edit-address/profile-edit-address.component';

import { SharedModule } from '@app/_shared/modules/shared.module';
import { ProfileAddAddressComponent } from './components/profile-add-address/profile-add-address.component'

import { AddressService } from '@app/_services';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule
  ],
  declarations: [
    ProfileComponent,
    ProfileOverviewComponent,
    ProfileEditAccountComponent,
    ProfileEditPasswordComponent,
    ProfileEditAddressComponent,
    ProfileAddAddressComponent
  ],
  providers: [
    AddressService
  ]
})
export class ProfileModule { }
