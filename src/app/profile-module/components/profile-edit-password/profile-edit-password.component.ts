import { Component, OnInit } from '@angular/core';
import { ProfileService } from '@app/_services';

declare const swal: any;

@Component({
  selector: 'app-profile-edit-password',
  templateUrl: './profile-edit-password.component.html',
  styleUrls: ['./profile-edit-password.component.scss']
})
export class ProfileEditPasswordComponent implements OnInit {
  pass_password = true;
  model: any = {};
  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit() {
  }

  last_step() {
    this.profileService.passwordResetStep2(this.model).subscribe(
      res => {
        swal('Password is changed.', 'success');
      },
      err => console.log(err)
    );
  }

  go_next() {
    this.profileService.passwordResetStep1(this.model.email).subscribe(
      res => this.pass_password = false,
      err => console.log(err)
    );
  }

  send(f) {
    if (this.pass_password) {
      return this.go_next();
    }
    this.last_step();
  }

}
