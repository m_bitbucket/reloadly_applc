import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AddressService } from '@app/_services';

declare const $: any;

@Component({
  selector: 'app-profile-add-address',
  templateUrl: './profile-add-address.component.html',
  styleUrls: ['./profile-add-address.component.scss']
})
export class ProfileAddAddressComponent implements OnInit, AfterViewInit {
  @Input() mode: boolean;
  @Input() model: any = {};
  @Output() change_mode = new EventEmitter();
  constructor(
    private addressService: AddressService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    $('#country_selector').countrySelect({
      // defaultCountry: "jp",
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      preferredCountries: ['ca', 'gb', 'us']
    });
  }

  send(f) {
    const country = $('#country_selector').countrySelect('getSelectedCountryData');
    console.log(country);
    console.log(this.model);
    // this.addressService.add_address(this.model).subscribe(
    //   res => console.log(res),
    //   err => console.log(err)
    // );
  }

  back() {
    this.change_mode.emit();
  }
}
