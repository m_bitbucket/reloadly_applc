import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAddAddressComponent } from './profile-add-address.component';

describe('ProfileAddAddressComponent', () => {
  let component: ProfileAddAddressComponent;
  let fixture: ComponentFixture<ProfileAddAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileAddAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileAddAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
