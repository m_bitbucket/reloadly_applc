import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileEditAddressComponent } from './profile-edit-address.component';

describe('ProfileEditAddressComponent', () => {
  let component: ProfileEditAddressComponent;
  let fixture: ComponentFixture<ProfileEditAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileEditAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEditAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
