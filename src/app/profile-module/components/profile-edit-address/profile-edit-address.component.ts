import { Component, OnInit, AfterViewInit } from '@angular/core';

import { AddressService } from '@app/_services';

declare const swal: any;
declare const $: any;

@Component({
  selector: 'app-profile-edit-address',
  templateUrl: './profile-edit-address.component.html',
  styleUrls: ['./profile-edit-address.component.scss']
})

export class ProfileEditAddressComponent implements OnInit {
  addresses: any = [];
  model: any = {};
  mode = false;
  current_model: any = {};
  constructor(
    private addressService: AddressService
  ) { }

  ngOnInit() {
    this.addressService.get_addresses().subscribe(
      (res: any) => this.addresses = res.content,
      err => console.log(err)
    );
  }

  send(f: HTMLFormElement) {
    console.log(this.model);
    swal('Saved!', '', 'success');
  }

  change_mode() {
    this.mode = !this.mode;
  }

  edit_adr(item) {
    this.current_model = item;
    this.change_mode();
  }
}
