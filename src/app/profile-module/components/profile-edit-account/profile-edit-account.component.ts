import { Component, OnInit, Input } from '@angular/core';
import { ProfileService } from '@app/_services';
declare const swal: any;

@Component({
  selector: 'app-profile-edit-account',
  templateUrl: './profile-edit-account.component.html',
  styleUrls: ['./profile-edit-account.component.scss']
})
export class ProfileEditAccountComponent implements OnInit {
  @Input() model;
  data = new FormData();
  file_name = 'Choose file...';
  err: string;
  constructor(
    private profileService: ProfileService
  ) {}

  ngOnInit() {

  }

  send(f: HTMLFormElement) {
    this.err = '';
    this.data.append('firstName', this.model.firstName);
    this.data.append('lastName', this.model.lastName);
    this.data.append('phone', this.model.phone.number);
    this.data.append('notificationStatus', this.model.notifications);

    this.profileService.update_profile(this.data).subscribe(
      res => {
        this.err = '';
        swal('Updated', '', 'success');
      },
      err => this.err = err.statusText
    );
  }

  change_file(e) {
    const file = e.target.files[0];
    this.data.append('avatar', file);
    this.file_name = file.name;
    // let reader = new FileReader();
  }
}
