import { Component, OnInit } from '@angular/core';

import { ProfileService } from '@app/_services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any = {};
  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    this.profileService.get_profile().subscribe(
      res => {
        this.user = res;
      },
      err => console.warn(err)
    );
  }

}
