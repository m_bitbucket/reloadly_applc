import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocsRoutingModule } from './docs-routing.module';

import { FormsModule } from '@angular/forms';
import { DocsComponent } from './components/docs/docs.component';


@NgModule({
  imports: [
    CommonModule,
    DocsRoutingModule,
    FormsModule
  ],
  declarations: [
    DocsComponent
  ]
})
export class DocsModule { }
