import { Component, OnInit } from '@angular/core';
declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.scss']
})
export class TopupComponent implements OnInit {
  model: any = {};


  constructor() { }

  ngOnInit() {

    $('.tab-wizard').steps({
      headerTag: 'h6'
      , bodyTag: 'section'
      , transitionEffect: 'fade'
      , titleTemplate: '<span class="step">#index#</span> #title#'
      , labels: {
        finish: 'Submit'
      }
      , onFinished: function (event, currentIndex) {
        swal('Thank you!', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.');
      }
    });

    $('#phone').intlTelInput({
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "./assets/js/utils.js"
    });
  }

}
