import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopupRoutingModule } from './topup-routing.module';
import { TopupComponent } from './components/topup/topup.component';

import { FormsModule } from '@angular/forms';





@NgModule({
  imports: [
    CommonModule,
    TopupRoutingModule,
    FormsModule
  ],
  declarations: [TopupComponent]
})
export class TopupModule { }
