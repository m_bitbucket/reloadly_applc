interface AuthConfig {
  clientID: string;
  domain: string;
  audience: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: '5WzNYCOnJSg873yXBe3QpPyTvejuqW2c',
  domain: 'reloadly-stage.auth0.com',
  audience: 'https://portal-stage.reloadly.com',
  callbackURL: 'http://localhost:3000',
};
